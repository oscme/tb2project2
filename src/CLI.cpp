#include "CLI.h"

CLI::CLI()
{
    dbm = new DatabaseManager();
    lexer = new Lexer();
    init();
}

void CLI::init(){
    bool exit = false;
    while(!exit){
        string input = getString();
        lexer->set_input(input);
        vector<string> entrance;
        try {
            entrance = lexer->build_vector();
        }catch (const std::invalid_argument& ia) {
            std::cerr << "Invalid format: " << ia.what() << '\n';
            continue;
        }
        if(entrance.size() ==0)
            continue;
        if(entrance[0] == "HELP"){

        }else if(entrance[0] == "CREATE"){

            if(validateEntranceLen(entrance, 2) && entrance[1] == "TABLE"){
                dbm->create_table(entrance);
            }else if(validateEntranceLen(entrance, 2) && entrance[1] == "DATABASE"){
                if(!validateEntranceLen(entrance,4)){
                    continue;
                }
                uint32 size=0;
                from_String_to_uint(entrance[3],&size);
                dbm->create_database(entrance[2], size);
            }else{
                printMsg("Command not supported.");
            }
        }else if(entrance[0] == "USE"){
            if(entrance.size() < 2){
                printMsg("Not enough arguments.");
                continue;
            }
            dbm->use_database(entrance[1]);
        }
        else if(entrance[0] == "DROP"){
            if(validateEntranceLen(entrance, 2) && entrance[1] == "TABLE"){
                try{
                    if(entrance.size() < 3){
                        printMsg("Not enough arguments.");
                        continue;
                    }
                    dbm->drop_table(entrance[2]);
                }catch (const std::invalid_argument& ia) {
                    std::cerr << ia.what() << '\n';
                    continue;
                }
            }else if(validateEntranceLen(entrance, 2) && entrance[1] == "DATABASE"){
                if(entrance.size() < 3){
                    printMsg("Not enough arguments.");
                    continue;
                }
                dbm->drop_database(entrance[2]);
            }else{
                printMsg("Command not supported.");
            }
        }
        else if(entrance[0] == "INSERT"){
            if(!validateEntranceLen(entrance,3)){
                continue;
            }
            try{
                dbm->insert_command(entrance);

            }catch (const std::invalid_argument& ia) {
                std::cerr << ia.what() << '\n';
                continue;
            }
        }else if(entrance[0] == "PRINT"){
            if(!validateEntranceLen(entrance,2))
                continue;
            if(entrance[1]== "DATABASE"){
                dbm->print_database_info();
            }else if(entrance[1] == "TABLE"){
                if(!validateEntranceLen(entrance,3))
                    continue;
                dbm->print_table_info(entrance[2]);
            }else{
                printMsg("Command not supported");
                continue;
            }
        }
        else if(entrance[0] == "UPDATE"){
            try{
                dbm->update_command(entrance);

            }catch (const std::invalid_argument& ia) {
                std::cerr << ia.what() << '\n';
                continue;
            }
        }
        else if(entrance[0] == "DELETE"){
            try{
                dbm->delete_command(entrance);

            }catch (const std::invalid_argument& ia) {
                std::cerr << ia.what() << '\n';
                continue;
            }
        }
        else if(entrance[0] == "SELECT"){
            try{
             dbm->select_command(entrance);

            }catch (const std::invalid_argument& ia) {
                std::cerr << ia.what() << '\n';
                continue;
            }
        }
        else if(entrance[0] == "EXIT"){
            exit = true;
        }else{
            cout<<"Command not supported."<<endl;
        }
    }
}

string CLI::getString(){

    cout<<"DBCLI/"<<dbm->use<<" >";
    string txt;
    vector<string> entrance;
    bool exit = false;
    string s ="";
    while(!exit){
        getline(cin, txt);
        if (txt.find(";") != std::string::npos){
            exit = true;
        }else
            txt.append(" ");
        s.append(txt);
    }
    return s;
}

CLI::~CLI()
{
    //dtor
}
